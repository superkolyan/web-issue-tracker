package net.zion54.webissuetracker.interfaces;

import net.zion54.webissuetracker.model.Comment;
import net.zion54.webissuetracker.model.Issues;
import net.zion54.webissuetracker.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Д on 24.07.2014.
 */
@Component
public interface IssueMapper {
    //Test query
    @Select("SELECT ISSUENAME,DATE,STATUS,INITIALCOMMENT,ISSUEID FROM ISSUES")
    @Results(value = {
            @Result(property = "issueName", column = "ISSUENAME"),
            @Result(property = "date", column = "DATE"),
            @Result(property = "status", column = "STATUS"),
            @Result(property = "initialComment", column = "INITIALCOMMENT"),
            @Result(property = "issueID", column = "ISSUEID")
    })
    public List<Issues> getIssueName();

    //insert new issue
    @Insert("INSERT INTO ISSUES VALUES(DEFAULT,#{issueName},#{date},#{createdByID},0,#{initialComment})")
    public int insert(Issues issues);

    //Select  issue by issueID
    @Select("SELECT ISSUENAME,STATUS,INITIALCOMMENT,CREATEDBYID,DATE FROM ISSUES WHERE ISSUEID=#{issueID}")
    @Results(value = {
            @Result(property = "issueName", column = "ISSUENAME"),
            @Result(property = "status", column = "STATUS"),
            @Result(property = "date", column = "DATE"),
            @Result(property = "initialComment", column = "INITIALCOMMENT"),
            @Result(property = "createdByID", column = "CREATEDBYID")
    })
    public Issues selectOneIssue(Issues issue);

    //Select all comments for target issue
    @Select("SELECT COMMENT,DATE,USERID FROM COMMENTS WHERE ISSUEID=#{issueID}")
    @Results(value = {
            @Result(property = "comment", column = "COMMENT"),
            @Result(property = "date", column = "DATE"),
            @Result(property = "createdByID", column = "USERID")

    })
    public List<Comment> selectCommentsById(Issues issues);

    //insert new comment for an issue
    @Insert("INSERT INTO COMMENTS VALUES(#{issueID},#{createdByID},#{date},#{comment})")
    public int insertComment(Comment comment);


    //select user by name(usernames are unique)
    @Select("SELECT USERID FROM USERS WHERE USERNAME=#{username} LIMIT 1")
    @Result(property = "userID", column = "USERID")
    public User selectUser(User user);

    //select user by id
    @Select("SELECT USERNAME FROM USERS WHERE USERID=#{userID} LIMIT 1")
    @Result(property = "username", column = "USERNAME")
    public User selectUserByID(User user);

    //update status of an issue
    @Update("UPDATE ISSUES SET STATUS=1 WHERE ISSUEID=#{issueID} ")
    public int updateIssueStatus(Comment comment);

}