package net.zion54.webissuetracker.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Д on 25.07.2014.
 */
public class Issues implements Serializable {
    private String issueName;
    private Date date;
    private boolean status;
    private String initialComment;
    private int issueID;
    private String creator;
    private int createdByID;

    public String getIssueName() {
        return issueName;
    }

    public void setIssueName(String issueName) {
        this.issueName = issueName;
    }

    public int getCreatedByID() {
        return createdByID;
    }

    public void setCreatedByID(int createdByID) {
        this.createdByID = createdByID;
    }


    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }


    public int getIssueID() {
        return issueID;
    }

    public void setIssueID(int issueId) {
        this.issueID = issueId;
    }


    public String getInitialComment() {
        return initialComment;
    }

    public void setInitialComment(String initialComment) {
        this.initialComment = initialComment;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String simplifyDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String s = sdf.format(date);
        return s;
    }

    public String toString() {
        return (issueName + " " + date + " " + status);
    }
}
