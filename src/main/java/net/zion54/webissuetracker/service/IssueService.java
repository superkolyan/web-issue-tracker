package net.zion54.webissuetracker.service;

import net.zion54.webissuetracker.model.Comment;
import net.zion54.webissuetracker.model.Issues;
import net.zion54.webissuetracker.model.User;

import java.util.List;

/**
 * Created by Д on 25.07.2014.
 */
public interface IssueService {
    public List<Issues> getIssueName();
    public int insert(Issues issues);
    public Issues selectOneIssue(Issues issue);
    public List<Comment> selectCommentsById(Issues issues);
    public int insertComment(Comment comment);
    public User selectUser(User user);
    public User selectUserByID(User user);
    public int updateIssueStatus(Comment comment);
}
