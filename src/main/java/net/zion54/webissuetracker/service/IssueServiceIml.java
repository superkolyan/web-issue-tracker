package net.zion54.webissuetracker.service;

import net.zion54.webissuetracker.interfaces.IssueMapper;
import net.zion54.webissuetracker.model.Comment;
import net.zion54.webissuetracker.model.Issues;
import net.zion54.webissuetracker.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Д on 25.07.2014.
 */
@Service("IssueService")
public class IssueServiceIml implements IssueService {
    @Autowired
     IssueMapper issueMapper;
    @Override
    public List<Issues> getIssueName() {
        List<Issues> issue= issueMapper.getIssueName();
        return issue;
    }

    @Override
    public int insert(Issues issues) {
        return issueMapper.insert(issues);
    }

    @Override
    public  Issues selectOneIssue(Issues issue) {
        return issueMapper.selectOneIssue(issue);
    }

    @Override
    public List<Comment> selectCommentsById(Issues issues) {
        List<Comment> comments= issueMapper.selectCommentsById(issues);
        return comments;
    }

    @Override
    public int insertComment( Comment comment) {
        return issueMapper.insertComment(comment);
    }

    @Override
    public User selectUser(User user) {
       return   issueMapper.selectUser(user);
    }

    @Override
    public User selectUserByID(User user) {
        return issueMapper.selectUserByID(user);
    }

    @Override
    public int updateIssueStatus(Comment comment) {
        return issueMapper.updateIssueStatus(comment);
    }
}
