package net.zion54.webissuetracker.controllers;

import net.zion54.webissuetracker.model.Comment;
import net.zion54.webissuetracker.model.Issues;
import net.zion54.webissuetracker.model.User;
import net.zion54.webissuetracker.service.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by Д on 24.07.2014.
 */
@Controller
@Component
public class MainController {
    @Autowired
    private IssueService issueService;

    @ModelAttribute("IssuesList")
    public List<Issues> prepareMSG() {
        return issueService.getIssueName();
    }

    @RequestMapping("/")
    public String root(Model model) {
        return "redirect:/index";
    }

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(Model model) {
        return "index";
    }

    @RequestMapping(value = "/createissue", method = RequestMethod.GET)
    public String createIssue(Model model) {
        model.addAttribute("issue", new Issues());
        return "createissue";
    }

    @RequestMapping(value = "/createissue", method = RequestMethod.POST)
    public String submitIssue(@ModelAttribute Issues issue, Model model, User user, Principal principal) {
        model.addAttribute("issue", issue);
        user.setUsername(principal.getName());
        user = issueService.selectUser(user);
        issue.setCreatedByID(user.getUserID());
        issue.setDate(new Date());
        issueService.insert(issue);
        return "redirect:/index";
    }

    @RequestMapping(value = "/viewissue", method = RequestMethod.GET)
    public String viewIssue(Issues issues, User user, Model model, @RequestParam(required = true) String id) {
        model.addAttribute("issueID", id);
        issues.setIssueID(Integer.valueOf(id));
        //selecting all comments for this issue
        List<Comment> comments = issueService.selectCommentsById(issues);
        issues = issueService.selectOneIssue(issues);
        user.setUserID(issues.getCreatedByID());
        user = issueService.selectUserByID(user);
        model.addAttribute("creationDate", issues.simplifyDate());
        model.addAttribute("creator", user.getUsername());
        model.addAttribute("issueName", issues.getIssueName());
        model.addAttribute("issueStatus", issues.isStatus());
        model.addAttribute("issueComment", issues.getInitialComment());
        //adding creator name for each comment
        for (Comment comment1 : comments) {
            user.setUserID(comment1.getCreatedByID());
            user = issueService.selectUserByID(user);
            comment1.setCreator(user.getUsername());
        }
        model.addAttribute("comments", comments);
        model.addAttribute("comment", new Comment());
        return "viewissue";
    }

    @RequestMapping(value = "/viewissue", method = RequestMethod.POST)
    public String submitCommentToissue(@ModelAttribute Comment comment, Model model, @RequestParam String id, Principal principal, User user) {
        model.addAttribute("comment", comment);
        comment.setIssueID(Integer.parseInt(id));
        user.setUsername(principal.getName());
        user = issueService.selectUser(user);
        comment.setCreatedByID(user.getUserID());
        comment.setDate(new Date());
        if (comment.isStatus()) {
            issueService.updateIssueStatus(comment);
            //BAD
            comment.setComment(comment.getComment() + " (changed to resolved) ");
        }
        issueService.insertComment(comment);
        return "redirect:viewissue?id=" + id;
    }
}
